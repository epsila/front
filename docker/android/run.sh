#!/bin/bash

cd /application
npm i

__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia emulator -port 5554 -memory 6144 -avd test -gpu host -no-boot-anim -no-snapshot &
wait-port localhost:5555
wait-port localhost:5554

socat tcp-listen:15555,bind=0.0.0.0,fork tcp:127.0.0.1:5555 &
socat tcp-listen:15554,bind=0.0.0.0,fork tcp:127.0.0.1:5554 &
socat tcp-listen:8000,bind=127.0.0.1,fork tcp:front:8000 &
socat tcp-listen:3000,bind=127.0.0.1,fork tcp:back:3000 &

while [ "`adb shell getprop sys.boot_completed | tr -d '\r' `" != "1" ] ; do sleep 1; done

adb reverse tcp:3000 tcp:3000
adb reverse tcp:8000 tcp:8000
cap run android --target emulator-5554 -l --host localhost --port 8000 --forwardPorts 3000:3000 --forwardPorts 8000:8000



sleep 10000000000000
