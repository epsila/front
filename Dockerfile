FROM node:20-bookworm

RUN npm install -g npm

USER node

COPY --chown=1000:1000 ./package.json /application/package.json
COPY --chown=1000:1000 ./package-lock.json /application/package-lock.json

WORKDIR /application

RUN npm install

COPY --chown=1000:1000 . /application

RUN npm run build

FROM nginx

COPY ./docker/nginx.conf /etc/nginx/conf.d/default.conf

COPY --from=0 --chown=1000:1000 /application/dist /usr/share/nginx/html
