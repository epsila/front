import { binaryToText, textToBinary } from "@/service/function";
import PubRsa from "./pubRsa.js";
import constante from "./constante";
import notepack from "notepack.io";

const getAESKeyByPassword = async (password, salt) => {
  if (!salt) {
    salt = crypto.getRandomValues(new Uint8Array(16));
  }
  const keyMaterial = await crypto.subtle.importKey(
    "raw",
    await textToBinary(password),
    "PBKDF2",
    false,
    ["deriveKey"],
  );
  let key = await crypto.subtle.deriveKey(
    {
      name: "PBKDF2",
      salt: salt,
      iterations: 100000,
      hash: "SHA-512",
    },
    keyMaterial,
    { name: "AES-GCM", length: 256 },
    false,
    ["wrapKey", "unwrapKey"],
  );
  return { salt, key };
};

let generateRSA;

// constructeur
const PriRsa = function () {
  PubRsa.call(this);
};

PriRsa.prototype.__proto__ = PubRsa.prototype;

// methode static

PriRsa.generateRSA = function () {
  generateRSA = self.crypto.subtle.generateKey(
    {
      ...constante.algoForEncript,
      modulusLength: "4096",
      publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
    },
    true,
    constante.keyUsagesForEncript.full,
  );
};
PriRsa.getRSA = async function (password) {
  const priRsa = new this();
  let publicKey, privateKey;
  if (generateRSA) {
    ({ publicKey, privateKey } = await generateRSA);
  } else {
    PriRsa.generateRSA();
    ({ publicKey, privateKey } = await generateRSA);
  }

  priRsa.binPublicKey = await self.crypto.subtle.exportKey(
    constante.formatBin.public,
    publicKey,
  );
  const binPrivateKey = await self.crypto.subtle.exportKey(
    constante.formatBin.private,
    privateKey,
  );
  priRsa.keyForEncript.publicKey = await self.crypto.subtle.importKey(
    constante.formatBin.public,
    priRsa.binPublicKey,
    constante.algoForEncript,
    true,
    constante.keyUsagesForEncript.public,
  );
  priRsa.keyForEncript.privateKey = await self.crypto.subtle.importKey(
    constante.formatBin.private,
    binPrivateKey,
    constante.algoForEncript,
    false,
    constante.keyUsagesForEncript.private,
  );

  priRsa.keyForSign.publicKey = await self.crypto.subtle.importKey(
    constante.formatBin.public,
    priRsa.binPublicKey,
    constante.algoForSign,
    false,
    constante.keyUsagesForSign.public,
  );
  priRsa.keyForSign.privateKey = await self.crypto.subtle.importKey(
    constante.formatBin.private,
    binPrivateKey,
    constante.algoForSign,
    false,
    constante.keyUsagesForSign.private,
  );
  generateRSA = undefined;
  const { key, salt } = await getAESKeyByPassword(password);
  let iv = self.crypto.getRandomValues(new Uint8Array(12));
  return {
    crypto: priRsa,
    exportKey: {
      publicKey: priRsa.binPublicKey,
      privateKey: await self.crypto.subtle.wrapKey(
        constante.formatBin.private,
        privateKey,
        key,
        {
          name: "AES-GCM",
          iv,
        },
      ),
      salt,
      iv,
    },
  };
};

PriRsa.import = async function ({ publicKey, privateKey, salt, iv }, password) {
  const priRsa = new this();

  const { key } = await getAESKeyByPassword(password, salt);
  priRsa.binPublicKey = publicKey;
  priRsa.keyForEncript.publicKey = await self.crypto.subtle.importKey(
    constante.formatBin.public,
    publicKey,
    constante.algoForEncript,
    true,
    constante.keyUsagesForEncript.public,
  );
  priRsa.keyForEncript.privateKey = await self.crypto.subtle.unwrapKey(
    constante.formatBin.private,
    privateKey,
    key,
    { name: "AES-GCM", iv },
    constante.algoForEncript,
    false,
    constante.keyUsagesForEncript.private,
  );

  priRsa.keyForSign.publicKey = await self.crypto.subtle.importKey(
    constante.formatBin.public,
    publicKey,
    constante.algoForSign,
    false,
    constante.keyUsagesForSign.public,
  );
  priRsa.keyForSign.privateKey = await self.crypto.subtle.unwrapKey(
    constante.formatBin.private,
    privateKey,
    key,
    { name: "AES-GCM", iv },
    constante.algoForSign,
    false,
    constante.keyUsagesForSign.private,
  );
  return priRsa;
};

// methode
PriRsa.prototype.signBin = async function (bin) {
  return self.crypto.subtle.sign(
    constante.algoForSign,
    this.keyForSign.privateKey,
    bin,
  );
};
PriRsa.prototype.signText = async function (text) {
  return await this.signBin(await textToBinary(text));
};

PriRsa.prototype.decryptBin = async function (message) {
  try {
    return await self.crypto.subtle.decrypt(
      constante.algoForEncript,
      this.keyForEncript.privateKey,
      message,
    );
  } catch (e) {
    throw new Error("error decript message");
    //return"lol";
  }
};
PriRsa.prototype.decryptObject = async function (message) {
  return await notepack.decode(await this.decryptBin(message));
};

PriRsa.prototype.decryptText = async function (message) {
  return await binaryToText(await this.decryptBin(message));
};

PriRsa.testPassword = async function ({ privateKey, salt, iv }, password) {
  try {
    const { key } = await getAESKeyByPassword(password, salt);

    await self.crypto.subtle.unwrapKey(
      constante.formatBin.private,
      privateKey,
      key,
      { name: "AES-GCM", iv },
      constante.algoForEncript,
      false,
      constante.keyUsagesForEncript.private,
    );
    return true;
  } catch (e) {
    console.error(e);
    return false;
  }
};

//     try {
//       await PriRsa.import(await get("keys"), password);
//     } catch (e) {
//       workerInInterface.postMessage("requestCreateDevice");
//       return;
//     }
// console.log(PriRsa)
// console.dir(PriRsa)

// PriRsa.getRSA("test").then(async ({crypto,exportKey})=>{

//   console.log(crypto);
//   console.log(exportKey);
//   const cryptoImport = await PriRsa.import(exportKey,"test")
//   console.log("import",cryptoImport)
//   const sign = await crypto.sign(await textToBinary("test?"));
//   console.log(sign);
//   const cryptoImportPublic = await PubRsa.import(exportKey.publicKey);
//   console.log(cryptoImportPublic);
//   console.log(await cryptoImportPublic.verify(sign,await textToBinary("test?")));
//   console.log(await crypto.verify(sign,await textToBinary("test?")));

//   const messageEncrypt = await cryptoImportPublic.encrypt("test !!!!!!!!!! ah!");
//   console.log(await crypto.decrypt(messageEncrypt));

// },console.error);
Object.freeze(PriRsa);
Object.freeze(PriRsa.prototype);

export default PriRsa;
