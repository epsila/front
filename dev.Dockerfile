FROM node:20-bookworm
RUN npm install -g npm

# RUN npm install -g vite @vue/cli
RUN rm -rf /tmp/*

USER node

WORKDIR /application/

CMD ["bash","-c","npm i && npm run dev"]
